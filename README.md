# OpenGL project

Faculty individual project done within the "Computer graphics" course.
Code repository: [github account](https://github.com/mi19117/RG_PROJEKAT)

**Build instructions:**
1. git clone https://github.com/mi19117/RG_PROJEKAT.git

2. Clion -> Open -> path/RG_PROJEKAT

3. Run -> Run 'project_base'


**Commands:**
1. W/S: Zoom in/Zoom out
2. A/D: moving left/right
3. Use arrows for car moving
4. V: spotlight on/off (only seen when the rotating diamond is below the surface)
5. press and hold K for SSAO

**Demo:**
https://www.youtube.com/watch?v=lvlPBqG9oPg&feature=youtu.be
